<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="IMG/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="CSS/contact.css">
    <title>Voyaneo</title>
</head>
<body>
    <nav>
        <a href="index.html"><img id="logo_svg" src="IMG/logo.svg" alt="image logo Voyaneo"></a>

        <div class="menu_nav">
        <div class="dropdown">
                    <button class="dropbtn">Continents</button>
                    <div class="dropdown-content">
                      <a href="NA/index.html">Amérique du nord</a>
                      <a href="SA/index.html">Amérique du sud</a>
                      <a href="AF/index.html">Afrique</a>
                      <a href="AS/index.html">Asie</a>
                      <a href="EU/index.html">Europe</a>
                    </div>
                  </div>
            <p><a href="partir.html">Où partir ?</a></p>
            <p><a href="devis.php">Demander un devis</a></p>
            <p><a href="about.html">Qui sommes-nous ?</a></p>
            <p><a href="contact.php">Contact</a></p>
        </div>

        <div class="burguer">
        <span id="bar1"></span>
        <span id="bar2"></span>
        </div>
    </nav>

    <section class="contact">

        <div class="information">
            <h1>Contactez-nous</h1>

            <div class="info">
                <img src="IMG/phone.png" alt="">
                <p>01.77.69.02.57</p>
            </div>
            <div class="info">
                <img src="IMG/mail.png" alt="">
                <p>info@voyaneo.com</p>
            </div>
            <div class="info">
                <img src="IMG/lieu.png" alt="">
                <p>163 avenue Charles de Gaulle 92200 <br>Neuilly sur Seine</p>
            </div>

        </div>

       

        <form method="POST" action="contact.php" class="form__contact">
            <h2>Envoyer un message</h2>
            <input type="email" name="email" id="email" placeholder="E-mail" required>
            <input type="text" name="telephone" id="telephone" placeholder="Téléphone" required>
            <textarea name="message" id="message" cols="30" rows="10" placeholder="Message" required></textarea>
            <div class="captcha">
            <div  class="g-recaptcha" data-sitekey="6Lc_y2waAAAAAEVXLRLvBfXo3HYSrZk1dqqAcYtZ"></div>
            </div>
            <button >
                Envoyer
                <img src="IMG/envoie.png" alt="logo envoie">
            </button>


            <?php
            require_once "recaptchalib.php";
            $secret = "6Lc_y2waAAAAACTwas4HxIqmoySXJGOVSPYzRt98";
 
            // empty response
            $response = null;
            
            // check secret key
            $reCaptcha = new ReCaptcha($secret);

            if(isset($_POST["g-recaptcha-response"])){

           

            if ($_POST["g-recaptcha-response"]) {
                $response = $reCaptcha->verifyResponse(
                    $_SERVER["REMOTE_ADDR"],
                    $_POST["g-recaptcha-response"]
                );
            }if ($response != null && $response->success) {



                if(!empty($_POST['telephone']) AND !empty($_POST['email']) AND !empty($_POST['message'])) {
                    $header="MIME-Version: 1.0\r\n";
                    $header.='Content-Type:text/html; charset="uft-8"'."\n";
                    $header.='Content-Transfer-Encoding: 8bit';
                    $message=  strip_tags($_POST['telephone'])." <br> ".strip_tags($_POST['email'])." <br> ".strip_tags(nl2br($_POST['message']));  
                            
                            
                                 
                    mail("serviceclient@voyaneo.com", "Demande de contact Voyaneo", $message, $header);
                    $msg="Votre message a bien été envoyé !";
                } 
                else {
                    $msg="Tous les champs doivent être complétés !";
                
                }

                echo "<strong><p style='color: green; font-family: sans-serif; '>Merci pour votre envoie !</p></strong>";
              } else {
                echo "<strong><p style='color: red; font-family: sans-serif;'>Veuillez remplir le captcha</p></strong>";
              }

            }
            ?>

        </form>
        

        
        
    </section>

    <footer>
        <img class="logo" src="./IMG/logo.svg" alt="logo voayneo">

        <ul>
            <li><a href="devis.php">Demander un devis</a></li>
            <li><a href="about.html">Qui sommes-nous ?</a></li>
            <li><a href="contact.php">Contact</a></li>
        </ul>

        <ul>
            <li><a href="mention.html">Conditions Générales</a></li>
            <li><a href="AX2020250-MULTIRISQUES-v3.pdf">Assurances Multirisque</a></li>
            <li><a href="AX2020251-ANNULATION-BAGAGES-v4.pdf">Annulation Bagages</a></li>
        </ul>
    </footer>

    <script>
        var Btn = document.querySelector('.menu_nav');
         var OpenBurguer = document.querySelector('.burguer');
        
         OpenBurguer.onclick = function(){
             Btn.classList.toggle('menu_nav_open');
         }
            </script>
        
            <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        
        <script>
           var swiper = new Swiper('.swiper-container', {
              pagination: {
                el: '.swiper-pagination',
                dynamicBullets: true,
              },
            });
        </script>
            <script src='https://www.google.com/recaptcha/api.js?hl=fr'></script>
    </body>
</html>

