document.addEventListener("DOMContentLoaded", () => {
  new Glide(".glide", {
    type: "carousel",
    startAt: 0,
    animationTimingFunc: "ease-in-out",
    gap: 100,
    perView: 3
  }).mount();

  let prevBtn = document.getElementById("prev");
  let nextBtn = document.getElementById("next");

  let background = document.querySelector(".background");
  let indices = document.querySelectorAll(".index");
  

  let bgImgs = ["espagne.webp", "norvege.webp", "grece.webp", "portugal.webp"];

  let currentIndex = 0;

  indices.forEach(index => index.classList.remove("active"));
  indices[currentIndex].classList.add("active");

  var myAnimation = new hoverEffect({
    parent: document.querySelector(".background"),
    // intensity: 0.3,
    imagesRatio: 1080 / 1920,
    image1: `img/${bgImgs[0]}`,
    image2: `img/${bgImgs[1]}`,
    // displacementImage: "img/14.jpg",
    // hover: false
  });

  var myAnimation2 = new hoverEffect({
    parent: document.querySelector(".background"),
    // intensity: 0.3,
    imagesRatio: 1080 / 1920,
    image1: `img/${bgImgs[1]}`,
    image2: `img/${bgImgs[2]}`,
    // displacementImage: "img/14.jpg",
    // hover: false
  });

  var myAnimation3 = new hoverEffect({
    parent: document.querySelector(".background"),
    // intensity: 0.3,
    imagesRatio: 1080 / 1920,
    image1: `img/${bgImgs[2]}`,
    image2: `img/${bgImgs[3]}`,
    // displacementImage: "img/14.jpg",
    // hover: false
  });

  var myAnimation4 = new hoverEffect({
    parent: document.querySelector(".background"),
    // intensity: 0.3,
    imagesRatio: 1080 / 1920,
    image1: `img/${bgImgs[3]}`,
    image2: `img/${bgImgs[0]}`,
    // displacementImage: "img/14.jpg",
    // hover: false
  });

  let distortAnimations = [
    myAnimation,
    myAnimation2,
    myAnimation3,
    myAnimation4
  ];

  function startNextDistortAnimation() {
    let prevIndex = currentIndex;
    currentIndex = (currentIndex + 1) % 4;
    indices.forEach(index => index.classList.remove("active"));
    indices[currentIndex].classList.add("active");
    distortAnimations[prevIndex].next();
    showTextAnimation("next");
    setTimeout(() => {
      let canvas = background.querySelectorAll("canvas");
      background.appendChild(canvas[0]);
      distortAnimations[prevIndex].previous();
    }, 1200);
  }

  function startPrevDistortAnimation() {
    currentIndex = currentIndex - 1 < 0 ? 3 : currentIndex - 1;
    indices.forEach(index => index.classList.remove("active"));
    indices[currentIndex].classList.add("active");
    distortAnimations[currentIndex].next();
    showTextAnimation("prev");
    setTimeout(() => {
      let canvas = background.querySelectorAll("canvas");
      background.insertBefore(canvas[canvas.length - 1], background.firstChild);
      distortAnimations[currentIndex].previous();
    }, 500);
  }

  nextBtn.addEventListener("click", () => {
    startNextDistortAnimation();

    // console.log(currentIndex);

    document.getElementById('thailand_content').style.display = (currentIndex == 0 ? 'block' : 'none');
    document.getElementById('japon_content').style.display = (currentIndex == 1 ? 'block' : 'none');
    document.getElementById('inde_content').style.display = (currentIndex == 2 ? 'block' : 'none');
    document.getElementById('vietnam_content').style.display = (currentIndex == 3 ? 'block' : 'none');

    document.getElementById('btnLien1').style.display = (currentIndex == 0 ? 'block' : 'none');
    document.getElementById('btnLien2').style.display = (currentIndex == 1 ? 'block' : 'none');
    document.getElementById('btnLien3').style.display = (currentIndex == 2 ? 'block' : 'none');
    document.getElementById('btnLien4').style.display = (currentIndex == 3 ? 'block' : 'none');


    
    
    
    
  });

  prevBtn.addEventListener("click", () => {
    startPrevDistortAnimation();

    // console.log(currentIndex);

    document.getElementById('thailand_content').style.display = (currentIndex == 0 ? 'block' : 'none');
    document.getElementById('japon_content').style.display = (currentIndex == 1 ? 'block' : 'none');
    document.getElementById('inde_content').style.display = (currentIndex == 2 ? 'block' : 'none');
    document.getElementById('vietnam_content').style.display = (currentIndex == 3 ? 'block' : 'none');

    document.getElementById('btnLien1').style.display = (currentIndex == 0 ? 'block' : 'none');
    document.getElementById('btnLien2').style.display = (currentIndex == 1 ? 'block' : 'none');
    document.getElementById('btnLien3').style.display = (currentIndex == 2 ? 'block' : 'none');
    document.getElementById('btnLien4').style.display = (currentIndex == 3 ? 'block' : 'none');


    
    
      
    console.log("canvas")
    document.querySelectorAll("canvas").style.display = (currentIndex == 0 ? 'block' : 'none')


  });

  let titleDisplacement = 0;
  let descriptionDisplacement = 0;

  function showTextAnimation(direction) {
    if (titleDisplacement === 0 && direction === "prev") {
      titleDisplacement = -540;
    } else if (titleDisplacement === -540 && direction === "next") {
      titleDisplacement = 0;
    } else {
      titleDisplacement =
        direction === "next"
          ? titleDisplacement - 180
          : titleDisplacement + 180;
    }

    if (descriptionDisplacement === 0 && direction === "prev") {
      descriptionDisplacement = -165;
    } 
    else if(descriptionDisplacement === -165 && direction === "next"){
      descriptionDisplacement = 0;
    }
    else {
      descriptionDisplacement =
        direction === "next" 
          ? descriptionDisplacement - 55
          : descriptionDisplacement + 55;
    }

    let title = document.querySelectorAll("#title h4");
    let description = document.querySelectorAll("#description p");

    title.forEach(title => {
      TweenMax.to(title, 1, {
        top: `${titleDisplacement}px`,
        ease: Strong.easeInOut
      });
    });

    description.forEach((description, index) => {
      let opacity = 0;
      if(index === currentIndex){
        opacity = 1;
      }else {
        opacity = 0;
      }
      TweenMax.to(description, 1, {
        top: `${descriptionDisplacement}px`,
        ease: Strong.easeInOut,
        opacity: opacity
      });
    })
  }
});


// JS FORMALITE

var btnform = document.querySelector('.btn_formalite');
var iframe = document.querySelector('.iframe_item');

var btnform1 = document.querySelector('.btn_formalite_1');
var iframe1 = document.querySelector('.iframe_item_1');

var btnform2 = document.querySelector('.btn_formalite_2');
var iframe2 = document.querySelector('.iframe_item_2');

var btnform3 = document.querySelector('.btn_formalite_3');
var iframe3 = document.querySelector('.iframe_item_3');



btnform.onclick = function(){
    iframe.classList.toggle('iframe_item_open');
}

btnform1.onclick = function(){
    iframe1.classList.toggle('iframe_item_open');
}

btnform2.onclick = function(){
    iframe2.classList.toggle('iframe_item_open');
}

btnform3.onclick = function(){
    iframe3.classList.toggle('iframe_item_open');
}



// JS LOADER


const loader = document.querySelector('.loader');

window.addEventListener('load', () => {
  loader.classList.add('fondu-out');
});


// JS NAV


var Btn = document.querySelector('.menu_nav');
        var OpenBurguer = document.querySelector('.burguer');
        
        OpenBurguer.onclick = function(){
            Btn.classList.toggle('menu_nav_open');
        }


// JS METEO


const APIKEY = '58d79fec889085b9538e1c0b5e109608';

let urlMadrid = `https://api.openweathermap.org/data/2.5/weather?q=Madrid&appid=${APIKEY}&units=metric&lang=fr`;
let urlBarcelone = `https://api.openweathermap.org/data/2.5/weather?q=Barcelone&appid=${APIKEY}&units=metric&lang=fr`;
let urlValencia = `https://api.openweathermap.org/data/2.5/weather?q=Seville&appid=${APIKEY}&units=metric&lang=fr`;
let urlSeville = `https://api.openweathermap.org/data/2.5/weather?q=Valence&appid=${APIKEY}&units=metric&lang=fr`;

        fetch(urlMadrid).then( response => 
            response.json().then((data) => {
                
                
                document.querySelector('#city1').innerHTML = Math.round( data.main.temp) +'°';
                
            
            })
            );
        fetch(urlBarcelone).then( response => 
            response.json().then((data) => {
                
                
                document.querySelector('#city2').innerHTML = Math.round( data.main.temp) +'°';
            
            })
            ); 
        fetch(urlSeville).then( response => 
            response.json().then((data) => {
                
                
                document.querySelector('#city3').innerHTML = Math.round( data.main.temp) +'°';
            
            })
            );
        fetch(urlValencia).then( response => 
            response.json().then((data) => {
                
                
                document.querySelector('#city4').innerHTML = Math.round( data.main.temp) +'°';
            
            })
            );   


            // fin Espagne api

            // Api Norvege

let urlOslo = `https://api.openweathermap.org/data/2.5/weather?q=Oslo&appid=${APIKEY}&units=metric&lang=fr`;
let urlBergen = `https://api.openweathermap.org/data/2.5/weather?q=Bergen&appid=${APIKEY}&units=metric&lang=fr`;
let urlTromso = `https://api.openweathermap.org/data/2.5/weather?q=Tromso&appid=${APIKEY}&units=metric&lang=fr`;
let urlTrondheim = `https://api.openweathermap.org/data/2.5/weather?q=Trondheim&appid=${APIKEY}&units=metric&lang=fr`;     




fetch(urlOslo).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city1-1').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlBergen).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city2-1').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    ); 
fetch(urlTromso).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city3-1').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlTrondheim).then( response => 
    response.json().then((data) => {
        
      
        document.querySelector('#city4-1').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );




let urlAthene = `https://api.openweathermap.org/data/2.5/weather?q=Athene&appid=${APIKEY}&units=metric&lang=fr`;
let urlMykonos = `https://api.openweathermap.org/data/2.5/weather?q=Mykonos&appid=${APIKEY}&units=metric&lang=fr`;
let urlOia = `https://api.openweathermap.org/data/2.5/weather?q=Oia&appid=${APIKEY}&units=metric&lang=fr`;
let urlCorfou = `https://api.openweathermap.org/data/2.5/weather?q=Corfou&appid=${APIKEY}&units=metric&lang=fr`;     




fetch(urlAthene).then( response => 
    response.json().then((data) => {
        
      
        document.querySelector('#city1-2').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlMykonos).then( response => 
    response.json().then((data) => {
        
       
        document.querySelector('#city2-2').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    ); 
fetch(urlOia).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city3-2').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlCorfou).then( response => 
    response.json().then((data) => {
        
      
        document.querySelector('#city4-2').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );



let urlLisbonne = `https://api.openweathermap.org/data/2.5/weather?q=Lisbonne&appid=${APIKEY}&units=metric&lang=fr`;
let urlSintra = `https://api.openweathermap.org/data/2.5/weather?q=Sintra&appid=${APIKEY}&units=metric&lang=fr`;
let urlMadere = `https://api.openweathermap.org/data/2.5/weather?q=Madère&appid=${APIKEY}&units=metric&lang=fr`;
let urlPorto = `https://api.openweathermap.org/data/2.5/weather?q=Porto&appid=${APIKEY}&units=metric&lang=fr`;     




fetch(urlLisbonne).then( response => 
    response.json().then((data) => {
        
       
        document.querySelector('#city1-3').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlSintra).then( response => 
    response.json().then((data) => {
        
      
        document.querySelector('#city2-3').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    ); 
fetch(urlPorto).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city3-3').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );
fetch(urlMadere).then( response => 
    response.json().then((data) => {
        
        
        document.querySelector('#city4-3').innerHTML = Math.round( data.main.temp) +'°';
    
    })
    );


          