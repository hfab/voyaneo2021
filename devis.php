<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="IMG/favicon.png" type="image/x-icon">
    <title>Voyaneo | Devis</title>
    <link rel="stylesheet" href="CSS/devis.css">
</head>
<body>

<nav>
        <a href="index.html"><img id="logo_svg" src="IMG/logo.svg" alt="image logo Voyaneo"></a>

        <div class="menu_nav">
        <div class="dropdown">
                  <button class="dropbtn">Continents</button>
                  <div class="dropdown-content">
                    <a href="NA/index.html">Amérique du nord</a>
                    <a href="SA/index.html">Amérique du sud</a>
                    <a href="AF/index.html">Afrique</a>
                    <a href="AS/index.html">Asie</a>
                    <a href="EU/index.html">Europe</a>
                  </div>
                </div>
            <p><a href="partir.html">Où partir ?</a></p>
            <p><a href="devis.php">Demander un devis</a></p>
            <p><a href="about.html">Qui sommes-nous ?</a></p>
            <p><a href="contact.php">Contact</a></p>
            
        </div>

        <div class="burguer">
        <span id="bar1"></span>
        <span id="bar2"></span>
        </div>
    </nav>


    <h1>Votre devis</h1>
<section class="section_form">
    <div class="block_form">



                <form id="regForm" action="devis.php" method="post">

            

            <!-- One "tab" for each step in the form: -->
            <div class="tab">
                  <h2>Destination</h2>
            
              <p>
                <label for="Pays">Pays</label>

                <?php 

                if(isset($_GET['pays'])){
                  echo('<input type="text" name="Pays" value="'.$_GET['pays'].'"  oninput="this.className = \'\'">');
                }
                else{
                  echo('<input type="text" name="Pays"  oninput="this.className = \'\'">');
                }

                 ?>
                
              </p>
              <p>
                <label for="Nbsp">Nombre de personnes</label>
                <input type="text" name="Nbsp"  oninput="this.className = ''">
              </p>
              <p>
              <label for="DateD">Date  de départ</label>
                <input type="Date" name="DateD" oninput="this.className = ''">
              </p>

              <p>
                <label for="DateR">Nombre de nuitées</label>
                <input type="text" name="DateR"  oninput="this.className = ''">
              </p>
              <p>
                <label for="Budget">Budget par personnes</label>
                <input type="text" name="Budget"  oninput="this.className = ''">
              </p>
              
            </div>

            <div class="tab">
              <h2>Information personnelles</h2>
              <p>
                <label for="Nom">Nom</label>
                <input type="text" name="Nom"  oninput="this.className = ''">
              </p>
              <p>
                <label for="Prénom">Prénom</label>
                <input type="text" name="Prénom"  oninput="this.className = ''">
              </p>
              <p>
                <label for="Tel">Téléphone</label>
                <input type="Tel" name="Tel"  oninput="this.className = ''">
              </p>
              <p>
                <label for="Mail">Adresse e-mail</label>
                <input type="email" name="Mail"  oninput="this.className = ''">
              </p>

              <div class="captcha">
                        <div  class="g-recaptcha" data-sitekey="6Lc_y2waAAAAAEVXLRLvBfXo3HYSrZk1dqqAcYtZ"></div>
              </div>
            </div>

            
            <div class="btn-group" style="overflow:auto;">
              <div style="float:right;">
                <button class="btn2" type="button" id="prevBtn" onclick="nextPrev(-1)">Retour</button>
                <button class="btn1" type="button" id="nextBtn" onclick="nextPrev(1)">Suivant</button>
              </div>
            </div>

            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
              <span class="step"></span>
              <span class="step"></span>
            </div>
            <?php

              require_once "recaptchalib.php";
              $secret = "6Lc_y2waAAAAACTwas4HxIqmoySXJGOVSPYzRt98";

              // empty response
              $response = null;

              // check secret key
              $reCaptcha = new ReCaptcha($secret);

              if(isset($_POST["g-recaptcha-response"])){



              if ($_POST["g-recaptcha-response"]) {
                  $response = $reCaptcha->verifyResponse(
                      $_SERVER["REMOTE_ADDR"],
                      $_POST["g-recaptcha-response"]
                  );
              }if ($response != null && $response->success) {

                  $header = "MIME-Version: 1.0\r\n";
                  $header .= 'Content-Type:text/html; charset="uft-8"'."\n";
                  $header .= 'Content-Transfer-Encoding: 8bit';
                  $sujet = 'Devis Voyaneo '.$_POST['Prénom'].' '.$_POST['Nom'].'';
                  $message = '
                  <html>
                      <body align="center">
                      <div style="width: 800px; background-color: #fdfdfd;">
                      <div  style="width:800px; height: 100px; background-color: #142639;">
                      <center><img style=" width: 200px; height: auto; margin-top: 30px;" src="https://cdn.shopify.com/s/files/1/0507/1005/3064/files/voyaneo.png?v=1613570220"></center>
                      </div>

                          <div style=" padding-top: 30px;" >
                          <h2>Informations Personnelles</h2>
                          <p>Nom : '.$_POST['Nom'].'</p>
                          <p>Prénom : '.$_POST['Prénom'].'</p>
                          <p>Téléphone : '.$_POST['Tel'].'</p>
                          <p>Email : '.$_POST['Mail'].'</p>
                          </div>

                          <div>
                          <h2>Informations Destination</h2>
                          <p>Pays : '.$_POST['Pays'].'</p> 
                          <p>Nombre de personnes : '.$_POST['Nbsp'].'</p> 
                          <p>Date de départ : '.$_POST['DateD'].'</p> 
                          <p>Nombre de nuitées : '.$_POST['DateR'].'</p> 
                          <p>Budget par personnes : '.$_POST['Budget'].' €</p> 
                          </div>


                          <div  style="width:800px; height: 100px; background-color: #142639;"></div>
                          </div>

                      </body>
                  </html>';
                  $retour = mail("yveschaponic@gmail.com", $sujet, $message, $header);


                  echo "<center><strong><p style='color: green; font-family: sans-serif; '>Merci pour votre demande !</p></strong></center>";
                } else {
                  echo "<center><strong><p style='color: red; font-family: sans-serif;'>Veuillez remplir le captcha</p></strong></center>";
                }

              }

  ?>

            </form> 

  
  </div>
    <div class="block_slide">
        <div class="slider">
      <div class="slides">
        <!--radio buttons start-->
        <input type="radio" name="radio-btn" id="radio1">
        <input type="radio" name="radio-btn" id="radio2">
        <!--radio buttons end-->
        <!--slide images start-->
        <div class="slide first">
          <img src="IMG/2-min.jpeg" alt="image1">
        </div>
        <div class="slide">
          <img src="IMG/1-min.jpeg" alt="image2">
        </div>
  
        <!--slide images end-->

        <!--automatic navigation start-->
        <div class="navigation-auto">
          <div class="auto-btn1"></div>
          <div class="auto-btn2"></div>
        </div>
        <!--automatic navigation end-->
      </div>

      <!--manual navigation start-->
      <div class="navigation-manual">
        <label for="radio1" class="manual-btn"></label>
        <label for="radio2" class="manual-btn"></label>
      </div>
      <!--manual navigation end-->
    </div>
    <!--image slider end-->

    </div>
</section>
    <footer>
            <img class="logo" src="./IMG/logo.svg" alt="logo voayneo">

            <ul>
                <li><a href="devis.php">Demander un devis</a></li>
                <li><a href="about.html">Qui sommes-nous ?</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>

            <ul>
                <li><a href="mention.html">Conditions Générales</a></li>
                <li><a href="AX2020250-MULTIRISQUES-v3.pdf">Assurances Multirisque</a></li>
                <li><a href="AX2020251-ANNULATION-BAGAGES-v4.pdf">Annulation Bagages</a></li>
            </ul>
        </footer>

    <script>
        var Btn = document.querySelector('.menu_nav');
        var OpenBurguer = document.querySelector('.burguer');

        OpenBurguer.onclick = function(){
        Btn.classList.toggle('menu_nav_open');
        }


        var counter = 1;
    setInterval(function(){
      
      if(counter < 4){
      document.getElementById('radio' + counter).checked = true;
      counter++;
      }
      else{
        counter= 0;
        counter++;

      }

    }, 3000);
    </script>

    <script>

        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
          // This function will display the specified tab of the form ...
          var x = document.getElementsByClassName("tab");
          x[n].style.display = "block";
          // ... and fix the Previous/Next buttons:
          if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
          } else {
            document.getElementById("prevBtn").style.display = "inline";
          }
          if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Envoyer";
          } else {
            document.getElementById("nextBtn").innerHTML = "Suivant";
          }
          // ... and run a function that displays the correct step indicator:
          fixStepIndicator(n)
        }

        function nextPrev(n) {
          // This function will figure out which tab to display
          var x = document.getElementsByClassName("tab");
          // Exit the function if any field in the current tab is invalid:
          if (n == 1 && !validateForm()) return false;
          // Hide the current tab:
          x[currentTab].style.display = "none";
          // Increase or decrease the current tab by 1:
          currentTab = currentTab + n;
          // if you have reached the end of the form... :
          if (currentTab >= x.length) {
            //...the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
          }
          // Otherwise, display the correct tab:
          showTab(currentTab);
        }

        function validateForm() {
          // This function deals with validation of the form fields
          var x, y, i, valid = true;
          x = document.getElementsByClassName("tab");
          y = x[currentTab].getElementsByTagName("input");
          // A loop that checks every input field in the current tab:
          for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
              // add an "invalid" class to the field:
              y[i].className += " invalid";
              // and set the current valid status to false:
              valid = false;
            }
          }

          // If the valid status is true, mark the step as finished and valid:
          if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
          }
          return valid; // return the valid status
        }

        function fixStepIndicator(n) {
          // This function removes the "active" class of all steps...
          var i, x = document.getElementsByClassName("step");
          for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
          }
          //... and adds the "active" class to the current step:
          x[n].className += " active";
        }
        
    </script>
    <script src="secu.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=fr'></script>
</body>
</html>